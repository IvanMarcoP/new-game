using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovimiento : MonoBehaviour
{
    public float senX;
    public float senY;

    public Transform orientacion;

    float rotacionX;
    float rotacionY;

    //Vector2 mouseMirar;
    //Vector2 suavidadV;

    //public float sensibilidad = 5.0f;
    //public float suavizado = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * senX;
        float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * senY;

        rotacionY += mouseX;
        rotacionX -= mouseY;

        rotacionX = Mathf.Clamp(rotacionX, -90f, 90f);

        transform.rotation = Quaternion.Euler(rotacionX, rotacionY, 0);

        orientacion.rotation = Quaternion.Euler(0, rotacionY, 0);

        //var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        //md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        //suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        //suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        //mouseMirar += suavidadV;
        //mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);

        //transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        //Jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Jugador.transform.up);

    }
}
