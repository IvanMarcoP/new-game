using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    private Rigidbody rb;
    Vector3 direcMovimiento;

    float inputHorizontal;
    float inputVertical;

    private float rapidezDesplazamiento;
    [Header("Movimiento")]
    public float caminarVelocidad;
    public float SprintVelocidad;


    [Header("Agacharse")]
    public float VelAgachado;
    public float escalaAgachadoY;
    private float startEscalaY;

    [Header("Salto")]
    public float fuerzaSalto;
    public float saltoCD;
    public float multiAire;
    bool puedeSalto;

    [Header("Teclas de Acceso")]
    public KeyCode Saltar = KeyCode.Space;
    public KeyCode Sprint = KeyCode.LeftShift;
    public KeyCode Crouch = KeyCode.LeftControl;


    //[Header("Slopes")]
    //public float AnguloSlope;
    //private RaycastHit SlopeHit;

    

    [Header("Estados")]
    bool piso;
    public float groundDrag;
    public float gravedad;
    public Transform orientacion;
    public LayerMask capaPiso;
    public float distanciaPiso;


    public EstadosMovimientos state;
    public enum EstadosMovimientos
    {
        caminar,
        sprintear,
        aire,
        agachado,
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Physics.gravity *= gravedad;
        startEscalaY = transform.localScale.y;

        puedeSalto = true;
    }

    // Update is called once per frame
    void Update()
    {
        //float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        //float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        //movimientoAdelanteAtras *= Time.deltaTime;
        //movimientoCostados *= Time.deltaTime;

        //transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        piso = Physics.Raycast(transform.position, Vector3.down, distanciaPiso * 0.5f + 0.2f, capaPiso);

        ControlVelocidad();
        MyInput();
        stateHandler();

        //if(OnSlope())
        //{
        //    rb.AddForce(GetSlopeMoveDirection(piso) * rapidezDesplazamiento * 20f, ForceMode.Force);
        //}

        if (piso)
        {
            rb.drag = groundDrag;
        }
        else
        {
            rb.drag = 2;
        }

        


    }

    private void FixedUpdate()
    {
        MoverPJ();
    }

    private void MyInput()
    {
        inputHorizontal = Input.GetAxisRaw("Horizontal");
        inputVertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKey(Saltar) && puedeSalto && piso)
        {
            puedeSalto = false;

            Salto();

            Invoke(nameof(ResetSalto), saltoCD);
        }

        if (Input.GetKey(Crouch))
        {
            transform.localScale = new Vector3(transform.localScale.x, escalaAgachadoY, transform.localScale.z);
            rb.AddForce(Vector3.down * 1.5f, ForceMode.Impulse);
        }

        if (Input.GetKeyUp(Crouch))
        {
            transform.localScale = new Vector3(transform.localScale.x, startEscalaY, transform.localScale.z);
        }
    
    }

    private void MoverPJ()
    {
        direcMovimiento = orientacion.forward * inputVertical + orientacion.right * inputHorizontal;

        if (piso)
        {
            rb.AddForce(direcMovimiento.normalized * rapidezDesplazamiento * 10f, ForceMode.Force);
        }
        else if (!piso)
        {
            rb.AddForce(direcMovimiento.normalized * rapidezDesplazamiento * 10f * multiAire, ForceMode.Force);
        }

    }

    private void stateHandler()
    {
        if (Input.GetKey(Crouch))
        {
            state = EstadosMovimientos.agachado;
            rapidezDesplazamiento = VelAgachado;
        }
         
        if (piso && Input.GetKey(Sprint))
        {
            state = EstadosMovimientos.sprintear;
            rapidezDesplazamiento = SprintVelocidad;
        }
        else if (piso)
        {
            state = EstadosMovimientos.caminar;
            rapidezDesplazamiento = caminarVelocidad;
        }
        else
        {
            state = EstadosMovimientos.aire;
        }
    }

    private void ControlVelocidad()
    {
        Vector3 velFlat = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        if (velFlat.magnitude > rapidezDesplazamiento)
        {
            Vector3 Limitador = velFlat.normalized * rapidezDesplazamiento;
            rb.velocity = new Vector3(Limitador.x, 0f, Limitador.z);
        }

    }

    private void Salto()
    {
        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
    }

    private void ResetSalto()
    {
        puedeSalto = true;
    }

    //private bool OnSlope()
    //{
    //    if (Physics.Raycast(transform.position, Vector3.down, out SlopeHit, distanciaPiso * 0.5f + 0.3f))
    //    {
    //        float angle = Vector3(Vector3.up, SlopeHit.normal);
    //        return angle < AnguloSlope && angle != 0;
    //    }

    //    return false;
    //}

    //private Vector3 GetSlopeMoveDirection()
    //{
    //    return Vector3.ProjectOnPlane(transform.position, SlopeHit.normal).normalized;
    //}

}
